//разрешить конфликт версий
var $jqStatusDocs = jQuery.noConflict(true);

$jqStatusDocs(document).ready(function(){
	var numberElementGeneral = 0;
	
	
	//обработчик submit формы
	$jqStatusDocs('#statusDocsForm').submit(function(){
		//перенапрвлять submit кнопки на click
		$jqStatusDocs("#sendNumberStatusBtn").click();
		return false;
	});
	
	$jqStatusDocs("#sendNumberStatusBtn").click(function(){
		
    	//Если все элементы заполненны и выбраны, то отправить форму на сервер
		if ($jqStatusDocs("#statusDocsLayout .light-error:visible").length == 0) {
			var numberElement = numberElementGeneral;
			var serialNumber = $jqStatusDocs("#serialNumber").val();
			var yearRegDoc = $jqStatusDocs("#yearRegDoc").val();
			
			//настройка ajax запроса
			$jqStatusDocs.ajaxSetup({
			
					url: serverUrl,
					data: {serialNumber: serialNumber, yearRegDoc: yearRegDoc},
			        async: true,
			        type:  "POST", 
			        
			        //beforeSubmit: function(arr, $jqStatusDocsform, options) {
			        beforeSend: function(XMLHttpRequest) {
			        	$jqStatusDocs(".statusDocsResult").prepend("<div id='num_element"+numberElement+"' class='loadStatus'>Заявление № "+serialNumber+"/СОО: Выполняется запрос. Подождите...</div>");
			        	return true;
			        },

			        success: function (data, textStatus) {
			        	$jqStatusDocs("#loadStatus").remove();
			        	var className;
			        	var msg;
			        	var statusCode = data; 
			        	
			        	if (statusCode == 004) {
			        		className='resultStatus errorMsg'; 
			        		msg='Неверный номер документа. Порядковый номер документа должен содержать только цифры.';
			        	} else if (statusCode == 005) {
			        		className='resultStatus errorMsg'; 
			        		msg='Документ не найден. Попробуйте подать запрос повторно, телефон для справок 78-79-01.';
			        	} else if (statusCode == 006) {
			        		className='resultStatus errorMsg'; 
			        		msg='Ошибка, телефон для справок 78-79-01.';
			        	} else if (statusCode == 040) {
			        		className='resultStatus succMsg'; 
			        		msg='Документ зарегистрирован, телефон для справок 78-79-01.';
			        	} else if (statusCode == 130) {
			        		className='resultStatus succMsg';
			        		msg='Вам необходимо предоставить дополнительные документы, телефон для справок 78-79-01.';
			        	} else if (statusCode == 110) {
			        		className='resultStatus succMsg';
			        		msg='Документ отозван заявителем, телефон для справок 78-79-01.';
			        	} else if (statusCode == 140) {
			        		className='resultStatus succMsg';
			        		msg='Документ находится на исполнении, телефон для справок 78-79-01.';
			        	} else if (statusCode == 150) {
			        		className='resultStatus succMsg';
			        		msg='Рассмотрение вашего заявления приостановлено, телефон для справок 78-79-01.';
			        	} else if (statusCode == 210) {
			        		className='resultStatus succMsg'; 
			        		msg='Подготовлен отказ в предоставлении документов, телефон для справок 78-79-01.';
			        	} else if (statusCode == 000) {
			        		className='resultStatus succMsg';
			        		msg='Документ отозван заявителем, телефон для справок 78-79-01.';
			        	} else if (statusCode == 200) {
			        		className='resultStatus succMsg';
			        		msg='Предоставлен отказ в получении документов, телефон для справок 78-79-01.';
			        	} else if (statusCode == 220) {
			        		className='resultStatus succMsg';
			        		msg='Документ подготовлен, телефон для справок 78-79-01.';
			        	} else if (statusCode == 300) {
			        		className='resultStatus succMsg';
			        		msg='Документ выдан на руки, телефон для справок 78-79-01.';
			        	} else if (statusCode == 310) {
			        		className='resultStatus succMsg';
			        		msg='Документ отправлен по почте, телефон для справок 78-79-01.';
			        	} else if (statusCode == 320) {
			        		className='resultStatus succMsg';
			        		msg='Ваше заявление перенаправлено на исполнение в иную организацию, телефон для справок 78-79-01.';
			        	} else if (statusCode == 330) {
			        		className='resultStatus succMsg';
			        		msg='Подготовленный  документ отправлен в личный кабинет заявителя на ПГУ, телефон для справок 78-79-01.';
			        	} else if (statusCode == 340) {
			        		className='resultStatus succMsg';
			        		msg='Подготовленный  документ отправлен по почте (исполнителем), телефон для справок 78-79-01.';
			        	} else if (statusCode == 350) {
			        		className='resultStatus succMsg';
			        		msg='Подготовленный  документ отправлен по электронной почте, телефон для справок 78-79-01.';
			        	} else if (statusCode == 360) {
			        		className='resultStatus succMsg';
			        		msg='Документ находится на выдаче в Службе одного окна, телефон для справок 78-79-01.';
			        	} else if (statusCode == 007) {
			        		className='resultStatus errorMsg';
			        		msg='Статус не определен, телефон для справок 78-79-01.';
			        	} else {
			        		className='resultStatus errorMsg'; 
			        		msg='Не найден код статуса, телефон для справок 78-79-01.';
			        	}
			        	
			        	num_element = $jqStatusDocs("#num_element"+numberElement);
			        	num_element.removeClass("loadStatus");
			        	num_element.addClass(className);
			        	num_element.html("Заявление № "+serialNumber+"/СОО: "+msg);
			        },

			        error: function (XMLHttpRequest, textStatus, errorThrown) {
			        	num_element = $jqStatusDocs("#num_element"+numberElement);
			        	num_element.removeClass("loadStatus");
			        	num_element.addClass("resultStatus");
			        	num_element.addClass("errorMsg");
			        	num_element.html("Заявление № "+serialNumber+"/СОО: Ошибка соединения с сервером.");
			        	
			        }
			        
			    });
			
			//выполнить ajax запрос
			$jqStatusDocs.ajax();
			numberElementGeneral++;
		}

		//очистить номер заявления
		$jqStatusDocs("#serialNumber").val('');
		
        //return false;
	});
});
