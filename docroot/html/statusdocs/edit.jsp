<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<portlet:defineObjects />
<jsp:useBean id="editBean" class="ru.admomsk.server.beans.EditBean" scope="request"/>

<div id="kindergarden">
	<form id="form_edit_status_docs" name="form_edit_status_docs" enctype="application/x-www-form-urlencoded" action="<portlet:renderURL />" method="post">
		<div class="form-all">
			
			<ul class="form-section">
				<li class="form-line">
					<label class="form-label-left">URL веб-сервиса СЭДД<span class="form-required">*</span></label>
					<div class="form-input-wide">
						<span class="form-sub-label-container">
							<input id="urlSEDD" name="urlSEDD" type="text" size="50" value="<c:out value="${editBean.url}"></c:out>" >
							<label class="form-sub-label">Введите URL, например, http://217.25.215.26/cm/sooweb.nsf/getdocstatus?OpenAgent</label>
							<label class="form-sub-label">Передаваемые параметры веб-сервиса:</label>
							<label class="form-sub-label">rnumber - порядковый номер документа без приставки и окончания</label>
							<label class="form-sub-label">rdate - год регистрации документа</label>
						</span>
					</div>
					<c:if test="${editBean.validUrl eq editBean.idle}">
		  				<div class="form-error-message for_host">
	  						Поле обязательно для заполнения.
	  					</div>
					</c:if>
				</li>
			</ul>
			
			<ul class="form-section">
				<li class="form-line">
					<div class="form-input-wide" id="cid_2">
						<div class="form-buttons-wrapper" style="text-align:left">
							<button class="form-submit-button" type="submit" id="save_info_different_areas">
							Сохранить
							</button>
						</div>
					</div>
				</li>
			</ul>
			
		</div>
	</form>
</div>