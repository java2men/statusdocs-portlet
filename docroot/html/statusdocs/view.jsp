<%--
/**
* Copyright (c) 2000-2010 Liferay, Inc. All rights reserved.
*
* This library is free software; you can redistribute it and/or modify it under
* the terms of the GNU Lesser General Public License as published by the Free
* Software Foundation; either version 2.1 of the License, or (at your option)
* any later version.
*
* This library is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
* details.
*/
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.GregorianCalendar" %>

<portlet:defineObjects />

<portlet:resourceURL var="serverUrl" id="serverUrl" />
<script type="text/javascript">
	var serverUrl = "<%= serverUrl %>"; // Name of the variable defined in jsp	
</script>


<div id="statusDocsLayout">
	<form accept-charset="utf-8" id="statusDocsForm"  enctype="multipart/form-data" method="post" action="<portlet:resourceURL></portlet:resourceURL>">
	
		<table>
			<thead>
				<tr>
					<th>Номер заявления:</th>
					<th>Год подачи:</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><input id="serialNumber" name="serialNumber" class="validInput" type="text">/СОО</td>
					<td><select id="yearRegDoc" name="yearRegDoc" class="validSelect">
						<%
						   	Calendar c = new GregorianCalendar();
							int limit = 2012;
							int year = c.get(Calendar.YEAR);
							//out.println("<option value=\"not value\">выберете год</option>");
							out.println("<option value="+year+" selected>"+year+"</option>");
							for (int y = year-1; y >= limit; y--) {
								out.println("<option value="+y+">"+y+"</option>");	
							}
					   	%>
					   	</select>
					</td>
				</tr>
			</tbody>
		</table>
		
	</form>
	<input id="sendNumberStatusBtn" name="sendNumberStatusBtn" type="button" value="Проверить">
	
	<div class='statusDocsResult'></div>
</div>