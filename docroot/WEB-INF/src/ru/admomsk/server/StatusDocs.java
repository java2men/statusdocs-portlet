package ru.admomsk.server;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.upload.UploadServletRequest;
import com.liferay.portal.util.PortalUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import ru.admomsk.server.beans.EditBean;

/**
 * Portlet implementation class StatusDocs
 */
public class StatusDocs extends GenericPortlet {

	protected String viewJSP;
	protected String editJSP;
	//url СЭДД
	protected String urlSEDD = "";
	
    private static Log _log = LogFactoryUtil.getLog("StatusDocs-portlet, class StatusDocs");
	
	
    public void init() {
        viewJSP = getInitParameter("view-jsp");
        editJSP = getInitParameter("edit-jsp");
    }
    
    public void doView(
            RenderRequest renderRequest, RenderResponse renderResponse)
        throws IOException, PortletException {
    	PortletSession portletSession = renderRequest.getPortletSession();
    	if ((EditBean)portletSession.getAttribute("editBean") == null){
    		portletSession.setAttribute("editBean", new EditBean());
    	}
    	//Установка настроек портлета
    	((EditBean)portletSession.getAttribute("editBean")).setPortletPreferences(renderRequest.getPreferences());
    	urlSEDD = ((EditBean)portletSession.getAttribute("editBean")).getUrl();
        include(viewJSP, renderRequest, renderResponse);
    }
    
    @Override
    protected void doEdit(RenderRequest renderRequest, RenderResponse renderResponse)
    		throws PortletException, IOException {
    	
    	PortletSession portletSession = renderRequest.getPortletSession();
    	
    	if ((EditBean)portletSession.getAttribute("editBean") == null){
    		portletSession.setAttribute("editBean", new EditBean());
    	}
    	
    	//Установка настроек портлета
    	((EditBean)portletSession.getAttribute("editBean")).setPortletPreferences(renderRequest.getPreferences());
		
    	if (renderRequest.getParameter("urlSEDD") != null) {
    		((EditBean)portletSession.getAttribute("editBean")).setUrl(renderRequest.getParameter("urlSEDD"));
    		((EditBean)portletSession.getAttribute("editBean")).validateUrl();
    	}
    	
		renderRequest.setAttribute("editBean", (EditBean)portletSession.getAttribute("editBean"));
    	
    	include(editJSP, renderRequest, renderResponse);
    }
    
    protected void include(
            String path, RenderRequest renderRequest,
            RenderResponse renderResponse)
        throws IOException, PortletException {

        PortletRequestDispatcher portletRequestDispatcher =
            getPortletContext().getRequestDispatcher(path);

        if (portletRequestDispatcher == null) {
            _log.error(path + " is not a valid include");
        }
        else {
            portletRequestDispatcher.include(renderRequest, renderResponse);
        }
    }
 
    @Override
    public void serveResource(ResourceRequest request, ResourceResponse response)
    		throws PortletException, IOException {
    	// TODO Auto-generated method stub
    	//HttpServletRequest hsr = PortalUtil.getHttpServletRequest(request);
    	//UploadServletRequest usr = PortalUtil.getUploadServletRequest(hsr);
    	
    	//String serialNumber = usr.getParameter("serialNumber");
    	//String  yearRegDoc = usr.getParameter("yearRegDoc");
    	
    	String serialNumber = request.getParameter("serialNumber");
    	String  yearRegDoc = request.getParameter("yearRegDoc");
    	
    	PortletSession portletSession = request.getPortletSession();
    	if ((EditBean)portletSession.getAttribute("editBean") == null){
    		portletSession.setAttribute("editBean", new EditBean());
    	}
    	
    	URL url = null;
    	InputStream is = null;
    	BufferedReader br = null;
    	StringBuffer sbResultString = null;
    	
    	try {
    		url = new URL(urlSEDD + "&rnumber="+serialNumber+"&rdate="+yearRegDoc);
    		br = new BufferedReader(new InputStreamReader(url.openStream(), "windows-1251"));
    		
    		String line;
            sbResultString = new StringBuffer();
            while ((line = br.readLine()) != null) {
            	sbResultString.append(line);
            }
			
    	} catch (Exception e) {
    		_log.error(e);
		} finally {
			if (is != null) is.close();
			if (br != null) br.close();
    	}
    	
    	String utf8Result = new String(sbResultString.toString().getBytes("UTF-8"), "UTF-8");
    	String statusCode = subStringWords(utf8Result, "<statuskod>", "</ statuskod>");
    	response.setCharacterEncoding("UTF-8");
    	//response.getWriter().println("statusCode='"+statusCode+"';");
    	response.getWriter().println(statusCode);
    }
    
    //поиск между словами
    private String subStringWords (String str, String str1, String str2) {
    	int begin = str.indexOf(str1)+str1.length();
    	int end = str.indexOf(str2)+1;
    	return str.substring(begin, end-1);
    }
    
}
