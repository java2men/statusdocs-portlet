package ru.admomsk.server.beans;

import java.io.IOException;
import java.io.Serializable;

import javax.portlet.PortletPreferences;
import javax.portlet.ReadOnlyException;
import javax.portlet.ValidatorException;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

public class EditBean implements Serializable {
	
	private static final long serialVersionUID = -2014204537198554574L;
	private static Log _log = LogFactoryUtil.getLog("StatusDocs-portlet, class EditBean");
	
	private PortletPreferences portletPreferences;
	
    private String url = "";
    private String validUrl = "";
	private String idle = "idle";
	private String ok = "ok";
	private boolean validAll;
	
    public PortletPreferences getPortletPreferences() {
		return portletPreferences;
	}
	public void setPortletPreferences(PortletPreferences portletPreferences) {
		this.portletPreferences = portletPreferences;
	}
		
	public String getUrl() {
		if (portletPreferences == null) return null;
		url = (String)portletPreferences.getValue("url", "");
		return url;
	}
	public void setUrl(String url) throws ReadOnlyException, ValidatorException, IOException {
		if (url == null) {
			url = "";
		}
		this.url = url;
		portletPreferences.setValue("url", this.url);
		portletPreferences.store();
	}
	
	public String getValidUrl() {
		return validUrl;
	}
	public void validateUrl() {
		if (url.equals("")) validUrl = idle;
		else validUrl = ok;
	}
	
	public String getIdle() {
		return idle;
	}
	public void setIdle(String idle) {
		this.idle = idle;
	}
	public String getOk() {
		return ok;
	}
	public void setOk(String ok) {
		this.ok = ok;
	}
	
	public boolean isValidAll() throws ReadOnlyException {
		getUrl();
		validateUrl();
		
		
		if (validUrl.equals(ok)) {
			validAll = true;
		} else {
			validAll = false;
		}
		
		return validAll;
	}
	public void setValidAll(boolean validAll) {
		this.validAll = validAll;
	}
    
}
